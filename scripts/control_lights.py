#!/usr/bin/env python
import rospy
import serial
from finch_bot.msg import traffic_light_cmd

class control_lights(object):

    def callback(self, data):
        self.ser.write(str(1000)) #start code
        self.ser.write(',')
        self.ser.write(str(data.numLights))
        self.ser.write(',')
        for d in data.rgb_array: #array of rgb values in that order
            self.ser.write(str(d))
            self.ser.write(',')

    def __init__(self, *args, **kwds):
        rospy.init_node('control_lights', anonymous=True)
        rospy.Subscriber('traffic_light_cmd', traffic_light_cmd, self.callback)
        self.ser = serial.Serial('/dev/ttyACM0', 9600)

        rospy.spin()

if __name__ == '__main__':
    cl = control_lights()
